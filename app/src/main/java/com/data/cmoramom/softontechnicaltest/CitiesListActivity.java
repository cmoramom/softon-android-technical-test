package com.data.cmoramom.softontechnicaltest;

import android.app.ProgressDialog;
import android.content.Intent;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import adapter.AdapterRV;
import adapter.City;

public class CitiesListActivity extends AppCompatActivity {
    //define the variable to store all city's data
    private ArrayList<City> cities = new ArrayList<>();
    private ProgressDialog dialog;

    private LocationManager locManager;
    private LocationListener locListener;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cities_list);
        setDialog();
        this.setTitle("Cities List");
        new Syncronization().execute();


    }


    private void setRV() {
        //this method will set the recicler view, the layout manager and the adapter as well
//recicler view instance
        final RecyclerView rv = (RecyclerView) findViewById(R.id.rv);
        rv.setHasFixedSize(true);
//define the layout manager as linerLayout
        LinearLayoutManager lm = new LinearLayoutManager(getApplicationContext());
        rv.setLayoutManager(lm);




        final AdapterRV adapter = new AdapterRV(cities);
        adapter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int i = rv.getChildAdapterPosition(v);
                City city = new City();
                city = cities.get(i);
//City parameters to pass to detail activity

                Intent intent = new Intent(CitiesListActivity.this, DetailCitiesActivity.class);
                intent.putExtra("cityName", city.getCityName());
                intent.putExtra("temperature", city.getTemperature());
                intent.putExtra("weatherDescr", city.getWeatherDescr());
                intent.putExtra("humidity", city.getHumidity());
                intent.putExtra("windSpeed", city.getWindSpeed());
                startActivity(intent);


            }
        });
        rv.setAdapter(adapter);


    }

    private void initializeDataCities(JSONObject resultData) {
        // This method creates an ArrayList that has the data collected from
        // the API, takes a JsonObject and assign the values into the City class

        String cityName = "";
        Double temperature = 0.00;
        String weatherDescr = "";
        Double humidity = 0.00;
        Double windSpeed = 0.00;
        cities = new ArrayList<>();

        if (resultData.length() > 0) {


            try {
                JSONArray citiesList = resultData.getJSONArray("list");
                for (int i = 0; i < citiesList.length(); i++) {

                    JSONObject citi = citiesList.getJSONObject(i);
                    JSONObject main = citi.getJSONObject("main");
                    JSONArray weather = citi.getJSONArray("weather");
                    JSONObject wind = citi.getJSONObject("wind");


                    cityName = citi.getString("name");
                    temperature = main.getDouble("temp");
                    weatherDescr = weather.getJSONObject(0).getString("description");
                    humidity = main.getDouble("humidity");
                    windSpeed = wind.getDouble("speed");

                    cities.add(new City(cityName, temperature, weatherDescr, humidity, windSpeed));


                }

            } catch (JSONException e) {
                e.printStackTrace();
            }


        } else {

            Toast.makeText(getApplicationContext(), "Error getting the list", Toast.LENGTH_LONG).show();
        }


    }


    private void setDialog() {
        dialog = new ProgressDialog(this);

        dialog.setMessage("Getting the  list of the cities nearby...");
        dialog.setTitle("Sync");
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setCancelable(false);


    }


    private class Syncronization extends AsyncTask<Double, Float, Integer> {

        @Override
        protected Integer doInBackground(Double... params) {

            //lat and lon are hardcode for the porpuse of this project, in real clients the values should be
            //obtain by GSP// lat, lon

            JSONObject result = new ConsumeAPI().getWheaterData(9.92, -84.14, 10);
            initializeDataCities(result);


            return null;
        }

        protected void onPreExecute() {
            // dialog.setProgress(0);
            // dialog.setMax(100);
            dialog.show(); //show a spinning dialog when task start
        }


        protected void onPostExecute(Integer result) {

            dialog.dismiss();  ///hide the spinning dialog when task has finished
            setRV();
            super.onPostExecute(result);

        }

    }
}











