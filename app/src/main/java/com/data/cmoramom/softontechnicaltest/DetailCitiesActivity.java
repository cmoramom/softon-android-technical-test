
package com.data.cmoramom.softontechnicaltest;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

public class DetailCitiesActivity extends AppCompatActivity {

    private TextView cityName;
    private TextView temperature;
    private TextView weatherDescr;
    private TextView humidity;
    private TextView windSpeed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_cities);


        cityName = (TextView) findViewById(R.id.cityName);
        temperature = (TextView) findViewById(R.id.temperature);
        weatherDescr = (TextView) findViewById(R.id.weatherDescr);
        humidity = (TextView) findViewById(R.id.humidity);
        windSpeed = (TextView) findViewById(R.id.windSpeed);

        //assined the parameters to the textview to show the weather's details

        Bundle bundle = getIntent().getExtras();


        cityName.setText("City: " + bundle.getString("cityName"));
        temperature.setText("Temperature: " + bundle.getDouble("temperature"));
        weatherDescr.setText("Weather Desc: " + bundle.getString("weatherDescr"));
        humidity.setText("Humidity: " + bundle.getDouble("humidity"));
        windSpeed.setText("Wind Speed: " + bundle.getDouble("windSpeed"));

        //set the activity title
        this.setTitle("Weather Details for " + bundle.getString("cityName"));


    }
}
