package com.data.cmoramom.softontechnicaltest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

/**
 * Created by cmoramom on 2/4/16.
 */
public class ConsumeAPI {

    private static final String APIKEY = "f7baa941e4459a4308486c5e1b2f0916";
    private final String HTTP_API_ = "http://api.openweathermap.org/data/2.5/find?";


    public JSONObject getWheaterData(double lat, double lon, int cnt) {
        JSONObject result = new JSONObject();


        String httpUrl = HTTP_API_ + "lat=" + lat + "&lon=" + lon + "&" + "&cnt=" + cnt + "&APPID=" + APIKEY;


        try {

            URL object = new URL(httpUrl);
            HttpURLConnection con = (HttpURLConnection) object.openConnection();
            con.setRequestProperty("Content-Type", "application/json");


//display what returns the POST request

            StringBuilder sb = new StringBuilder();
            int HttpResult = con.getResponseCode();
            if (HttpResult == HttpURLConnection.HTTP_OK) {
                BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"));
                String line = null;
                while ((line = br.readLine()) != null) {
                    sb.append(line + "\n");
                }

                br.close();

                try {
                    result = new JSONObject(new String(sb));


                } catch (JSONException e) {


                /*try {
                   // JSONObject jSonResult = new JSONObject(new String(sb));
                    //token = jSonResult.getString("token");
                } catch (JSONException el) {
                    el.printStackTrace();
                }*/
                }


            } else {

            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();

        } catch (MalformedURLException e) {
            e.printStackTrace();

        } catch (ProtocolException e) {
            e.printStackTrace();

        } catch (IOException e) {
            e.printStackTrace();

        }


        return result;
    }


}
