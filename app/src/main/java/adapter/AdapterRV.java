package adapter;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.data.cmoramom.softontechnicaltest.R;

import java.util.List;

/**
 * Created by cmoramom on 2/4/16.
 */
public class AdapterRV extends RecyclerView.Adapter<AdapterRV.CitiesViewHolder> implements View.OnClickListener {

    //this list will handle the city's data
    List<City> cities;

    private View.OnClickListener listener;

    public AdapterRV(List<City> cities) {


        this.cities = cities;


    }

    @Override
    public void onClick(View v) {
        if (listener != null)
            listener.onClick(v);

    }


    public static class CitiesViewHolder extends RecyclerView.ViewHolder {

        //defined the variables to load the city's data
        CardView cv;
        TextView citiName;
        TextView citiTemperature;


        CitiesViewHolder(View itemView) {
            super(itemView);
            cv = (CardView) itemView.findViewById(R.id.cv);
            citiName = (TextView) itemView.findViewById(R.id.citi_name);
            citiTemperature = (TextView) itemView.findViewById(R.id.citi_temperature);

        }
    }

    @Override

    public CitiesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cities_list_card_view, parent, false);
        v.setOnClickListener(this);
        CitiesViewHolder cvh = new CitiesViewHolder(v);
        return cvh;

    }

    @Override
    public void onBindViewHolder(CitiesViewHolder holder, int i) {

        holder.citiName.setText(cities.get(i).getCityName());
        holder.citiTemperature.setText("Temp: " + cities.get(i).getTemperature().toString());


    }

    @Override
    public int getItemCount() {
        return cities.size();
    }

    public void setOnClickListener(View.OnClickListener listener) {
        this.listener = listener;
    }


}




