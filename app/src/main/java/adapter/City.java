package adapter;

/**
 * Created by cmoramom on 2/4/16.
 */
public class City {

    private String cityName;
    private Double temperature;
    private String weatherDescr;
    private Double humidity;
    private Double windSpeed;

    public City() {
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getWeatherDescr() {
        return weatherDescr;
    }

    public void setWeatherDescr(String weatherDescr) {
        this.weatherDescr = weatherDescr;
    }

    public Double getHumidity() {
        return humidity;
    }

    public void setHumidity(Double humidity) {
        this.humidity = humidity;
    }

    public Double getWindSpeed() {
        return windSpeed;
    }

    public void setWindSpeed(Double windSpeed) {
        this.windSpeed = windSpeed;
    }

    public City(String cityName, Double temperature, String weatherDescr, Double humidity, Double windSpeed) {
        this.cityName = cityName;
        this.temperature = temperature;
        this.weatherDescr = weatherDescr;
        this.humidity = humidity;
        this.windSpeed = windSpeed;
    }

    public Double getTemperature() {
        return temperature;
    }

    public void setTemperature(Double temperature) {
        this.temperature = temperature;
    }
}
